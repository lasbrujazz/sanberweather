import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import * as firebase from 'firebase'
import apiKeys from './config/keys'

import WelcomeScreen from './screens/WelcomeScreen'
import SignUp from './screens/SignUp'
import SignIn from './screens/SignIn'
import LoadingScreen from './screens/LoadingScreen'
import Dashboard from './screens/Dashboard'
import Weather from './screens/Weather'
import About from './screens/About'

const Stack = createStackNavigator()

export default function App() {
  console.reportErrorsAsExceptions = false

  if (!firebase.apps.length) {
    console.log('Connected with Firebase')
    firebase.initializeApp(apiKeys.firebaseConfig)
  }

  const horizontalAnimation = {
    gestureDirection: 'horizontal',
    cardStyleInterpolator: ({ current, layouts }) => {
      return {
        cardStyle: {
          transform: [
            {
              translateX: current.progress.interpolate({
                inputRange: [0, 1],
                outputRange: [layouts.screen.width, 0],
              }),
            },
          ],
        },
      };
    },
  };

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={horizontalAnimation}>
        <Stack.Screen name={'Loading'} component={LoadingScreen} options={{headerShown: false}} />
        <Stack.Screen name='Awal' component={WelcomeScreen} options={{headerShown: false}} />
        <Stack.Screen name='Daftar' component={SignUp} options={{headerShown: false}} />
        <Stack.Screen name='Masuk' component={SignIn} options={{headerShown: false}} />
        <Stack.Screen name={'Depan'} component={Dashboard} options={{headerShown: false}} />
        <Stack.Screen name='Cuaca' component={Weather} />
        <Stack.Screen name='Tentang' component={About} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
