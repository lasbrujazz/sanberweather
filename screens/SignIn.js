import React, { useState } from 'react'
import { View, Text, TextInput, StyleSheet, Alert, Image, TouchableHighlight, TouchableOpacity } from 'react-native'
import { signIn } from '../API/firebaseMethods'
import styles from './styles'

export default function SignIn() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const handlePress = () => {
        if (!email) {
            Alert.alert('Email is required!')
        }

        if (!password) {
            Alert.alert('Password is required!')
        }

        signIn(email, password)
        setEmail('')
        setPassword('')
    }

    return (
        <View style={styles.containerSignIn}>
            <View style={styles.splitSignIn1}>
                <Image style={styles.iconSignIn} source={require('../assets/icon_top.png')} />
                <Text style={styles.textSignIn}>Masuk ke akun anda</Text>
            </View>
            <View style={styles.splitSignIn2}>
                <TextInput
                    style={styles.formInputSignIn}
                    placeholder='Email'
                    value={email}
                    onChangeText={(email) => setEmail(email)}
                    autoCapitalize='none'
                />
                <TextInput
                    style={styles.formInputSignIn}
                    placeholder='Kata sandi'
                    value={password}
                    onChangeText={(password) => setPassword(password)}
                    secureTextEntry={true}
                />
                <TouchableOpacity style={styles.buttonSignIn} onPress={handlePress}>
                    <Text style={styles.buttonTextSignIn}>Masuk</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}