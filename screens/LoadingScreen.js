import React, { useEffect } from 'react'
import { ActivityIndicator, StyleSheet, View } from 'react-native'
import * as firebase from 'firebase'
import styles from './styles'

export default function LoadingScreen ({navigation}) {
    useEffect(() => {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                navigation.replace('Depan')
            } else {
                navigation.replace('Awal')
            }
        })
    })

    return (
        <View style={styles.containerLoading}>
            <ActivityIndicator size='large' />
        </View>
    )
}