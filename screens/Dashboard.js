import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, Alert, LogBox, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import * as firebase from 'firebase'
import { loggingOut } from '../API/firebaseMethods'
import axios from 'axios'
import styles from './styles'

export default function Dashboard ({route, navigation}) {
    console.reportErrorsAsExceptions = false
    LogBox.ignoreAllLogs()

    let currentUserUID = firebase.auth().currentUser.uid
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [mail, setMail] = useState('')
    const [quote, setQuote] = useState('')

    const [imgIndex, setImgIndex] = useState('')
    const [imgFile, setImgFile] = useState(require('../assets/2.jpg'))

    const [weather, setWeather] = useState('')
    const [location, setLocation] = useState('Salatiga')

    useEffect(() => {
        async function getUserInfo() {
            let doc = await firebase.firestore().collection('users').doc(currentUserUID).get()

            if (!doc.exists) {
                Alert.alert('No user data found!')
            } else {  
                let dataObj = doc.data()
                setFirstName(dataObj.firstName)
                setLastName(dataObj.lastName)
                setMail(dataObj.email)
                setQuote(dataObj.quote)
                setImgIndex(dataObj.image)
            }
        }
        getWeather() 
        getUserInfo()
        imgNum()
    }, [])

    const getWeather = () => {
        axios.get(`https://goweather.herokuapp.com/weather/${location}`)
            .then(res => {
                setWeather(res.data)
            })
    }
    
    const imgNum = () => {
        switch(imgIndex) {
            case 1:
                setImgFile(require('../assets/1.jpg'))
                break;
            case 2:
                setImgFile(require('../assets/2.jpg'))
                break;
            default:
                setImgFile(require('../assets/3.jpg'))
                break;
        }
    }

    const weatherDetails = () => {
        navigation.navigate('Cuaca', {weather: weather, location: location})
    }

    const aboutMe = () => {
        navigation.navigate('Tentang', {firstName: firstName, lastName: lastName, mail: mail, quote: quote, imgIndex: imgIndex})
    }

    return (
        <View style={styles.containerDashboard}>
            <View style={styles.cardDashboard1}>
                <View style={styles.profileDashboard}>
                    <Image style={{height: 125, width: 125, borderRadius: 100}} source={imgFile} />
                    <View style={{alignItems: 'flex-end'}}>
                        <Text style={styles.titleTextDashboard}>Halo kembali,</Text>
                        <Text style={styles.nameDashboard}>{firstName}</Text>
                    </View>
                </View>
                <View>
                    <TouchableOpacity style={styles.buttonDashboard} onPress={aboutMe}>
                        <Text style={styles.buttonTextDashboard}>Lihat Profil</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.cardDashboard2}>
                <View>
                    <View>
                        <Text style={styles.textDashboard}>Suhu di {location} hari ini berkisar {weather.temperature}.</Text>
                    </View>
                    <TouchableOpacity style={styles.buttonDashboard} onPress={weatherDetails}>
                            <Text style={styles.buttonTextDashboard}>Detail Cuaca</Text>
                        </TouchableOpacity>
                </View>
                <View style={{padding: 30}}>
                    <TouchableOpacity style={styles.buttonDashboard} onPress={loggingOut}>
                        <Text style={styles.buttonTextDashboard}>Keluar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}