import React, { useState, useEffect } from 'react'
import { View, Text, Image, FlatList } from 'react-native'
import styles from './styles'

// image provided by snipstock.com

export default function Weather({route, navigation}) {
    const {weather, location} = route.params
    const [cuaca, setCuaca] = useState('')
    const [tes, setTes] = useState('')
    const [image, setImage] = useState(require('../assets/favicon.png'))

    const weatherConversion = () => {
        switch(tes) {
            case 'Clear':
                setCuaca('Cerah')
                setImage(require('../assets/weather_cerah.png'))
                break;
            case 'Sunny':
                setCuaca('Terang')
                setImage(require('../assets/weather_cerah.png'))
                break;
            case 'Partly cloudy':
                setCuaca('Sedikit berawan')
                setImage(require('../assets/weather_cerahberawan.png'))
                break;
            case 'Cloudy':
                setCuaca('Berawan')
                setImage(require('../assets/weather_berawan.png'))
                break;
            case 'Overcast':
                setCuaca('Mendung')
                setImage(require('../assets/weather_mendung.png'))
                break;
            case 'Mist':
                setCuaca('Berkabut')
                setImage(require('../assets/weather_mendung.png'))
                break;
            case 'Patchy rain possible':
                setCuaca('Kemungkinan hujan rintik-rintik')
                setImage(require('../assets/weather_gerimis.png'))
                break;
            case 'Patchy snow possible':
                setCuaca('Kemungkinan turun salju')
                setImage(require('../assets/weather_gerimis.png'))
                break;
            case 'Patchy sleet possible':
                setCuaca('Kemungkinan hujan es')
                setImage(require('../assets/weather_gerimis.png'))
                break;
            case 'Patchy freezing drizzle possible':
                setCuaca('Kemungkinan hujan sangat dingin')
                setImage(require('../assets/weather_hujanlebat.png'))
                break;
            case 'Thundery outbreaks possible':
                setCuaca('Kemungkinan langit penuh petir')
                setImage(require('../assets/weather_hujanpetir.png'))
                break;
            case 'Blowing snow':
                setCuaca('Angin disertai salju')
                setImage(require('../assets/weather_gerimis.png'))
                break;
            case 'Blizzard':
                setCuaca('Badai salju')
                setImage(require('../assets/weather_gerimis.png'))
                break;
            case 'Fog':
                setCuaca('Berkabut')
                setImage(require('../assets/weather_mendung.png'))
                break;
            case 'Freezing fog':
                setCuaca('Kabut suhu dingin')
                setImage(require('../assets/weather_mendung.png'))
                break;
            case 'Patchy light drizzle':
                setCuaca('Gerimis ringan')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Light drizzle':
                setCuaca('Gerimis')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Freezing drizzle':
                setCuaca('Gerimis dengan suhu dingin')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Heavy freezing drizzle':
                setCuaca('Gerimis lebat dengan suhu dingin')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Patchy light rain':
                setCuaca('Hujan ringan rintik-rintik')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Light rain':
                setCuaca('Hujan ringan')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Moderate rain at times':
                setCuaca('Hujan sedang berkepanjangan')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Moderate rain':
                setCuaca('Hujan sedang')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Heavy rain at times':
                setCuaca('Hujan lebat berkepanjangan')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Heavy rain':
                setCuaca('Hujan lebat')
                setImage(require('../assets/weather_hujanlebat.png'))
                break;
            case 'Light freezing rain':
                setCuaca('Hujan ringan dengan suhu dingin')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Moderate or heavy freezing rain':
                setCuaca('Hujan lebat dengan suhu dingin')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Light sleet':
                setCuaca('Hujan es ringan')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Moderate or heavy sleet':
                setCuaca('Hujan es sedang atau lebat')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Patchy light snow':
                setCuaca('Hujan salju rintik-rintik')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Light snow':
                setCuaca('Hujan salju ringan')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Patchy moderate snow':
                setCuaca('Hujan salju sedang rintik-rintik')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Moderate snow':
                setCuaca('Hujan salju sedang')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Patchy heavy snow':
                setCuaca('Hujan salju lebat rintik-rintik')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Heavy snow':
                setCuaca('Hujan salju lebat')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Ice pellets':
                setCuaca('Hujan es')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Light rain shower':
                setCuaca('Gerimis mengundang')
                setImage(require('../assets/weather_gerimis.png'))
                break;
            case 'Moderate or heavy rain shower':
                setCuaca('Gerimis sedang atau lebat')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Torrential rain shower':
                setCuaca('Gerimis lokal')
                setImage(require('../assets/weather_gerimis.png'))
                break;
            case 'Light sleet showers':
                setCuaca('Hujan es kecil')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Moderate or heavy sleet showers':
                setCuaca('Hujan es sedang atau lebat')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Light snow showers':
                setCuaca('Hujan salju ringan')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Moderate or heavy snow showers':
                setCuaca('Hujan salju sedang atau lebat')
                setImage(require('../assets/weather_hujanringan.png'))
                break;
            case 'Patchy light rain with thunder':
                setCuaca('Gerimis dengan petir')
                setImage(require('../assets/weather_hujanpetir.png'))
                break;
            case 'Moderate or heavy rain with thunder':
                setCuaca('Gerimis lebat dengan petir')
                setImage(require('../assets/weather_hujanpetir.png'))
                break;
            case 'Patchy light snow with thunder':
                setCuaca('Hujan salju ringan dengan petir')
                setImage(require('../assets/weather_hujanpetir.png'))
                break;
            case 'Moderate or heavy snow with thunder':
                setCuaca('Hujan salju lebat dengan petir')
                setImage(require('../assets/weather_hujanpetir.png'))
                break;
            default:
                setCuaca(weather.description)
                setImage(require('../assets/favicon.png'))
                break;
        }
    }

    useEffect(() => {
        setTes(weather.description)
        weatherConversion()
    })

    return (
        <View style={styles.containerWeather}>
            <View style={{flex: 4, justifyContent: 'center'}}>
                <Text style={styles.textWeather}>Cuaca di {location} hari ini adalah :</Text>
                <Text style={styles.textWeather}>{cuaca}</Text>
                <Text style={styles.textWeather}>dengan perkiraan suhu {weather.temperature}</Text>
                <Image source={image} />
            </View>
            <View style={{flex: 3}}>
                <Text style={styles.textWeather}>Prakiraan cuaca untuk hari esok :</Text>
                <FlatList
                    data={weather.forecast}
                    renderItem={({item}) => (
                        <View style={styles.flatListWeather}>
                            <Text style={styles.textWeatherDetail}>Hari ke-{item.day}</Text>
                            <Text style={styles.textWeatherDetail}>Suhu {item.temperature} dengan kecepatan angin {item.wind}</Text>
                        </View>
                    )}
                />
            </View>
        </View>
    )
}

