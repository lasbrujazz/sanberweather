import React, { useState, useEffect } from 'react'
import { View, Text, Alert, Image } from 'react-native'
import * as firebase from 'firebase'
import styles from './styles'

export default function About({route, navigation}) {
    const {firstName, lastName, mail, quote, imgIndex} = route.params

    const [imgFile, setImgFile] = useState(require('../assets/3.jpg'))
    
    const imgNum = () => {
        switch(imgIndex) {
            case 1:
                setImgFile(require('../assets/1.jpg'))
                break;
            case 2:
                setImgFile(require('../assets/2.jpg'))
                break;
            default:
                setImgFile(require('../assets/3.jpg'))
                break;
        }
    }

    useEffect(() => {
        imgNum()
    })

    return (
        <View style={styles.containerAbout}>
            <Image style={{height: 200, width: 200, marginBottom: 40}} source={imgFile} />
            <Text style={styles.textAbout}>Hello, {firstName} {lastName}</Text>
            <Text style={styles.textAbout}>{mail}</Text>
            <Text style={styles.textAbout}>{quote}</Text>
        </View>
    )
}