import { View, Text, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import styles from './styles'

export default function WelcomeScreen ({navigation}) {
    return (
        <View style={styles.containerWelcome}>
            <Image style={styles.iconWelcome} source={require('../assets/icon_top.png')} />
            <View style={styles.containerWelcome2}>
                <TouchableOpacity style={styles.buttonWelcome} onPress={() => navigation.navigate('Daftar')}>
                    <Text style={styles.buttonTextWelcome}>Daftar</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.buttonWelcome} onPress={() => navigation.navigate('Masuk')}>
                    <Text style={styles.buttonTextWelcome}>Masuk</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}