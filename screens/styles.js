import React from 'react'
import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    // WelcomeScreen.js
    containerWelcome: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    containerWelcome2: {
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
    backgroundWelcome: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonWelcome: {
        width: 150,
        borderRadius: 15,
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: '#C5CAE9',
        padding: 5,
        margin: '2%'
    },
    buttonTextWelcome: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'black'
    },
    inlineTextWelcome: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'navy',
        textAlign: 'center',
        marginTop: '5%'
    },
    titleWelcome: {
        fontSize: 235,
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'center'
    },
    titleContainerWelcome: {
        position: 'absolute',
        top: 170
    },
    iconWelcome: {
        height: 300,
        width: 300,
        margin: 25
    },

    // SignUp.js
    containerSignUp: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    splitSignUp1: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    splitSignUp2: {
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonSignUp: {
        width: 200,
        padding: 5,
        backgroundColor: '#C5CAE9',
        borderWidth: 2,
        borderColor: 'black',
        borderRadius: 15,
        alignSelf: 'center',
        margin: '5%'
    },
    buttonTextSignUp: {
        fontSize: 20,
        color: 'black',
        fontWeight: 'bold',
        textAlign: 'center'
    },
    outlineTextSignUp: {
        fontSize: 20,
        color: 'black',
        textAlign: 'center',
        marginTop: '5%'
    },
    inlineTextSignUp: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        textAlign: 'center',
        marginTop: '5%'
    },
    textSignUp: {
        textAlign: 'center',
        fontSize: 25,
        fontWeight: 'bold',
        color: 'black'
    },
    textInputSignUp: {
        width: 300,
        fontSize: 18,
        backgroundColor : '#C5CAE9',
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 10,
        padding: 10,
        margin: 5
    },
    iconSignUp: {
        height: 150,
        width: 150
    },
    signInTickerSignUp: {
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center', 
        margin: 20
    },

    // SignIn.js
    buttonSignIn: {
        width: 200,
        padding: 5,
        backgroundColor: '#C5CAE9',
        borderWidth: 2,
        borderColor: 'black',
        borderRadius: 15,
        alignSelf: 'center',
        margin: '5%'
    },
    buttonTextSignIn: {
        fontSize: 20,
        color: 'black',
        fontWeight: 'bold',
        textAlign: 'center'
    },
    containerSignIn: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    formInputSignIn: {
        width: 300,
        fontSize: 18,
        backgroundColor : '#C5CAE9',
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 10,
        padding: 10,
        margin: 5
    },
    textSignIn: {
        textAlign: 'center',
        fontSize: 25,
        fontWeight: 'bold',
        color: 'black'
    },
    iconSignIn: {
        height: 150,
        width: 150
    },
    splitSignIn1: {
        flex: 2, 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    splitSignIn2: {
        flex: 3, 
        alignItems: 'center', 
        justifyContent: 'flex-start'
    },

    // Dashboard.js
    buttonDashboard: {
        width: 200,
        padding: 5,
        backgroundColor: '#C5CAE9',
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 15,
        alignSelf: 'center'
    },
    buttonTextDashboard: {
        fontSize: 20,
        color: 'black',
        fontWeight: 'bold',
        textAlign: 'center'
    },
    containerDashboard: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    nameDashboard: {
        textAlign: 'center',
        fontSize: 20,
        marginTop: '2%',
        marginBottom: '10%',
        marginRight: '3%',
        fontWeight: 'bold',
        color: 'black'
    },
    textDashboard: {
        textAlign: 'center',
        fontSize: 20,
        fontStyle: 'italic',
        marginTop: '2%',
        marginBottom: '4%',
        fontWeight: 'bold',
        color: 'black'
    },
    titleTextDashboard: {
        textAlign: 'center',
        fontSize: 30,
        fontWeight: 'bold',
        color: '#2E6194'
    },
    cardDashboard1: {
        flex: 1,
        alignSelf: 'stretch',
        paddingTop: 50
    },
    cardDashboard2: {
        flex: 2,
        alignSelf: 'stretch',
        paddingTop: 50,
        justifyContent: 'space-between'
    },
    profileDashboard: {
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between', 
        paddingLeft: 20, 
        paddingRight: 20, 
        paddingBottom: 30
    },

    // Weather.js
    containerWeather: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textWeather: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black'
    },
    textWeatherDetail: {
        textAlign: 'center',
        fontSize: 18,
        color: 'black'
    },
    flatListWeather: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginTop: 15
    },

    // About.js
    containerAbout: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textAbout: {
        textAlign: 'center',
        fontSize: 20,
        fontStyle: 'italic',
        marginTop: '2%',
        marginBottom: '10%',
        fontWeight: 'bold',
        color: 'black'
    },

    // LoadingScreen.js
    containerLoading: {
        height: '100%',
        width: '100%',
        backgroundColor: '#3FC5AB',
        alignItems: 'center',
        justifyContent: 'center'
    },
})

export default styles