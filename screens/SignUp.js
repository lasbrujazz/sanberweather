import React, { useState } from 'react'
import { View, Text, TextInput, Alert, ScrollView, Keyboard, StyleSheet, SafeAreaView, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { registration } from '../API/firebaseMethods'
import styles from './styles'

export default function SignUp ({navigation}) {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')

    const emptyState = () => {
        setFirstName('')
        setLastName('')
        setEmail('')
        setPassword('')
        setConfirmPassword('')
    }

    const handlePress = () => {
        if (!firstName) {
            Alert.alert('First name is required!')
        } else if (!email) {
            Alert.alert('Email is required!')
        } else if (!password) {
            Alert.alert('Password is required!')
        } else if (!confirmPassword) {
            setPassword('')
        } else if (password !== confirmPassword) {
            Alert.alert('Password does not match!')
        } else {
            registration (
                email, password, lastName, firstName
            )
            navigation.navigate('Loading')
            emptyState()
        }
    }

    return (
        <View style={styles.containerSignUp}>
            <View style={styles.splitSignUp1}>
                <Image style={styles.iconSignUp} source={require('../assets/icon_top.png')} />
                <Text style={styles.textSignUp}>Buat akun anda</Text>
            </View>
            <View style={styles.splitSignUp2}>
                <ScrollView>
                    <TextInput
                        style={styles.textInputSignUp}
                        placeholder='Nama awal'
                        value={firstName}
                        onChangeText={(name1) => setFirstName(name1)}
                    />
                    <TextInput
                        style={styles.textInputSignUp}
                        placeholder='Nama akhir'
                        value={lastName}
                        onChangeText={(name2) => setLastName(name2)}
                    />
                    <TextInput
                        style={styles.textInputSignUp}
                        placeholder='Email'
                        value={email}
                        onChangeText={(email) => setEmail(email)}
                    />
                    <TextInput
                        style={styles.textInputSignUp}
                        placeholder='Kata sandi'
                        value={password}
                        onChangeText={(password1) => setPassword(password1)}
                        secureTextEntry={true}
                    />
                    <TextInput
                        style={styles.textInputSignUp}
                        placeholder='Konfirmasi kata sandi'
                        value={confirmPassword}
                        onChangeText={(password2) => setConfirmPassword(password2)}
                        secureTextEntry={true}
                    />
                    <TouchableOpacity style={styles.buttonSignUp} onPress={handlePress}>
                        <Text style={styles.buttonTextSignUp}>Daftar</Text>
                    </TouchableOpacity>
                    <View style={styles.signInTickerSignUp}>
                        <Text style={styles.outlineTextSignUp}>Sudah punya akun? </Text>
                        <Text style={styles.inlineTextSignUp} onPress={() => navigation.navigate('Masuk')}>Masuk</Text>
                    </View>
                </ScrollView>
            </View>
        </View>
    )
}